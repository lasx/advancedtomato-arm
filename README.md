# AndreDVJ's AdvancedTomato-ARM #

Forked off from Jacky's AdvancedTomato-ARM (which is a fork of Tomato-ARM by Shibby).

This is my personal fork, of the firmware I am currently running in my router, where I attempt to fix issues, make small adjustments, update open-source components if possible, and cherry-pick fixes from others.
This repository, while I try to keep aligned to Shibby's as much as possible, it has diverted already as some components are newer versions.

Some of the changes are functionally pointless, but may offer potential bug and security fixes.
Firmware size increased around 1MB compared to Shibby AIO builds. Should not be an issue to routers with or more than 32MB of flash memory.

I only have ARM routers: Netgear R7000 and R8000 (My WNR3500Lv2 died, was MIPS anyway), so I'm unable to test or attempt to provide specific support to another router.

If you see any issues or want to request a specific build, please let me know.

If anyone wants to pick up my changes and merge them to your repository, feel free and go ahead. That's the reason Tomato is an open-source project.